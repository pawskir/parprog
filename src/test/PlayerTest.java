package test;

import burr.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class PlayerTest {
    private int nrOfPLayers;
    private int divider;
    private int nrOfRuns;
    private ArrayList<String> names;
    private GameRules gr;
    private Player player;


    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Setup class for this test.
     */
    @Before
    public void setUp(){
        names = new ArrayList<>();
        names.add("Adam");
        names.add("Bertil");
        names.add("Chris");

        nrOfPLayers = 3;
        divider = 13;
        nrOfRuns = 100;
        gr = new GameRules(nrOfPLayers, divider, nrOfRuns, names);
        player = new Player(gr,100, divider, "Kalle");
    }

    /**
     * Tears down after completed tests.
     */
    @After
    public void tearDown() {
        gr = null;
        player = null;
    }

    /**
     * Testing that the correct form of the string is returned.
     */
    @Test
    public void testReturnString() {
        String res = player.play();

        // Den har sortens assert kraver att man har med "-ea" som argument till JVM:en, annars misslyckas ALDRIG
        // testerna. Nagon av oss ska komma ihag att ta med det i presentationen! :)
        assert "Kalle: 1".equals(res) : "Expected correct return string, got " + res;

        // For att den har assertmetoden (och manga andra som assertNotNull, assertSame...) ska funka maste man
        // gora import static org.testng.AssertJUnit.*; Den ar annars smidigare, eftersom den automatiskt skriver ut
        // expected och actual. Och for att man slipper sitta och undra varfor testerna aldrig misslyckas nar man
        // har glomt "-ea"...
        //assertEquals("Player 100: 1", res);
    }
}