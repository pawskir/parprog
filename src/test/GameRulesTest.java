package test;

import burr.GameRules;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class GameRulesTest {
    private int nrOfPLayers;
    private int divider;
    private int nrOfRuns;
    private ArrayList<String> names;
    private GameRules gr;

    /**
     * Setup class for this test.
     */
    @Before
    public void setUp(){
        names = new ArrayList<>();
        names.add("Adam");
        names.add("Bertil");
        names.add("Chris");

        nrOfPLayers = 3;
        divider = 13;
        nrOfRuns = 100;
        gr = new GameRules(nrOfPLayers, divider, nrOfRuns, names);
    }

    /**
     * Tears down class after completed tests.
     */
    @After
    public void tearDown() {
        gr = null;
    }

    /**
     * The current number must be a positive one to make the game work.
     */
    @Test
    public void testCurrentNumber(){
        assert (gr.getCurrentNumber()>0);
    }

    /**
     * The increaseNumber method is used to
     */
    @Test
    public void testIncreaseCurrentNumber() {
        int cn = gr.getCurrentNumber();
        gr.increaseCurrentNumber();
        assertEquals(gr.getCurrentNumber(),cn+1);
    }
}