package burr;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class UI {
    private int inputPlayers;

    public UI(){
        inputPlayers = 0;
    }

    public int inputPlayers(){
        inputPlayers = Integer.parseInt(JOptionPane.showInputDialog("Ange antalet spelare: "));
        return inputPlayers;
    }

    public int inputDivider(){
        return Integer.parseInt(JOptionPane.showInputDialog("Ange siffra för burr: "));
    }

    public int inputRuns(){
        return Integer.parseInt(JOptionPane.showInputDialog("Ange max antal rundor: "));
    }

    public ArrayList<String> inputNames(){
        ArrayList<String> playerNames = new ArrayList<>();
        for (int i = 0; i < inputPlayers; i++) {
            playerNames.add(JOptionPane.showInputDialog("Ange namn på spelare " + (i+1) ));
        }
        return playerNames;
    }

    public void print(String s){

        JTextArea textArea = new JTextArea(s);
        JScrollPane scrollPane = new JScrollPane(textArea);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        scrollPane.setPreferredSize( new Dimension( 250, 500 ) );
        JOptionPane.showMessageDialog(null, scrollPane, "Resultat",
                JOptionPane.YES_NO_OPTION);;
    }

}
