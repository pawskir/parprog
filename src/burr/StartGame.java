package burr;

/**
 * Created 2019-10-21
 */
public class StartGame {
    public void start() {
        UI ui = new UI();
        GameRules gr = new GameRules(ui.inputPlayers(), ui.inputDivider(), ui.inputRuns(), ui.inputNames());
        ui.print(gr.startGame());

    }
}