package burr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created 2019-10-21
 */
public class GameRules {

    
    //Instance fields
    
    private int currentNumber;
    private int currentPlayer;
    private int playUntilThisNumber;
    private int dividerToSkip;
    private List<Player> playerList;
    private ArrayList<String> playerNames;

    /**
    * Constructor
    * @param numberOfPlayers
    * @param divider
    */
    public GameRules(int numberOfPlayers, int divider, int nrOfRuns, ArrayList<String> names){
        playerList = new ArrayList<>();
        currentNumber = 1;
        currentPlayer = numberOfPlayers;
        playUntilThisNumber = nrOfRuns;
        dividerToSkip = divider;
        playerNames = names;
        fillPlayerList(numberOfPlayers, names);
    }
    /**
    * @return Returns the answer.
    */
    public int getTheAnswer() {
        return 42;
    }

    /**
    * @return Returns the number that is about to be "said" by one of the players.
    */
    public int getCurrentNumber() {
        return currentNumber;
    }

    public ArrayList<String> getPlayerNames() {
        return playerNames;
    }

    /**
    *Increases the number to be said by one.
    */
    public void increaseCurrentNumber() {
        this.currentNumber++;
    }

    /**
    * Fills a list of players after the parameters given from the MindRoad.main class.
    * 
    * @param names
    */
    private void fillPlayerList(int nrOfPlayers, ArrayList<String> names){
        for(int i = 0 ; i < nrOfPlayers ; i++){
            Player p = new Player(this, i+1, dividerToSkip, names.get(i));
            playerList.add(p);
        }
    }

    /**
    * Lets the players take turns to play the game of "Burr", passing
    * the turn between players until the playUntilThisNumber is reached.
     */
    public String startGame(){
        String s = "";
        while(currentNumber<=playUntilThisNumber){
            s += playerList.get(currentPlayer++% playerList.size()).play() + "\n";
        }
        return s;
    }
}