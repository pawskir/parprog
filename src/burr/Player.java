package burr;



/**
 * Created 2019-10-21
 */
public class Player {

    //Instance fields
    
    private GameRules rules;
    private int dividerToSkip;
    private int playerNumber;
    private String playerName;

    /**
    * Constructor
    * @param r - rules for the game
    * @param playerNr - which player in turn
    * @param divider - divider for the game
     * @param name - name of the player
    */
    public Player(GameRules r, int playerNr, int divider, String name){
        rules = r;
        dividerToSkip = divider;
        playerNumber = playerNr;
        playerName = name;
    }

    /**
    * Checks the GameRule class for info on the game's state
    * @return  a string depending on the outcome.
     */
    public String play() {
        String s;
        if(rules.getCurrentNumber()%dividerToSkip==0){
            s = playerName + ": Burr";
        }
        else {
            s = playerName + ": " +rules.getCurrentNumber();
        }
        rules.increaseCurrentNumber();
        return s;
    }
}